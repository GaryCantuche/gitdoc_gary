/**
*aquesta classe representa a la persona 
*@author Gary Cantuche
*@version 1.0
*/
public class Persona {

private String nom;
public String cognom;

private final String nomComplet;
private float posicio;
/**
*Aquest és el constructor de la classe. Assgina els valors dels atributs.
*@param _nom - ens indica el nom de la persona
*@param _cognom - ens indica el cognom de la persona
*/
public Persona(String _nom, String _cognom) {
nomComplet = nom + " " + cognom;
nom = _nom;
cognom = _cognom;
}

public void caminar(float _distancia) {
posicio += moure(_distancia);
}

protected float moure(float _distancia) {
return _distancia * 1.0f;
}

public void parlar(String _missatge) {
System.out.println(generarMissatge(_missatge));
}

private String generarMissatge(String _missatge) {
return nomComplet + " (" + posicio + "m): " + _missatge;
}
}
